/* The header file */

/* Fuction to see if the value of the pixel is in the circle or not. */
int InCircle(int totalRows, int totalCols, int radius, int pixRow, int pixCol);
int InStem(int totalrows, int totalcols, int radius, int pixRow, int pixCol);
int InMouth(int totalrows, int totalcols, int radius, int pixRow, int pixCol);
int InNose(int totalrows, int totalcols, int radius, int pixRow, int PixCol);
