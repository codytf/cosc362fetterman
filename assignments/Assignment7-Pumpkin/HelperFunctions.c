#include <math.h> // Allows me to use the pow(x,y) --> x^y

/* This is a bunch of stuff to use in pixel mapping. */

int InCircle(int totalRows, int totalCols, int radius, int pixRow, int pixCol) {
    int InOrOut = 0; // Integer flag for if the current pixel is in the circle or not: 1 is yes, 0 is no.

    int centerX = totalCols / 2;
    int centerY = totalRows / 2;
    int dist    = 0;              //Distance from center to pixel.
    int eyedist = 0;
    int eyedist2 = 0;

    dist = pow((pow(centerX - pixCol,2)) + (pow(centerY - pixRow,2)), 0.5);
    eyedist = pow((pow((centerX / 1.35) - pixCol,2)) + (pow((centerY / 1.75) - pixRow,2)), 0.5);
    eyedist2 = pow((pow((centerX * 1.25) - pixCol,2)) + (pow((centerY / 1.75) - pixRow,2)) , 0.5);

    if (dist < radius) {
        InOrOut = 1;
    }

    if(eyedist < (radius / 8)) {
	    InOrOut = 2;
    }  
    
    if (eyedist2 < (radius / 8)) {
	  InOrOut = 2;
    }

    return InOrOut;
}

int InStem(int totalrows, int totalcols, int radius, int pixRow, int pixCol) {
    int InOrOut = 0; //Integer flag for being in the stem.
          
    int brow = (totalrows / 2) - radius;
    int trow = (totalrows / 2) - (radius + 25);

    int lcol = (totalcols / 2) - 15;
    int rcol = (totalcols / 2) + 15;
    
    if(pixRow > trow && pixRow < brow) {
	if(pixCol > lcol && pixCol < rcol) {
           InOrOut = 2;
	}
    }

   return InOrOut;    
}

int InMouth(int totalrows, int totalcols, int radius, int pixRow, int pixCol) {
    int InOrOut = 0;

    int brow = (totalrows / 2.5) + (radius / 2);
    int trow = (totalrows / 2.5) + ((radius / 2) + 25);

    int lcol = (totalcols / 2) - 30;
    int rcol = (totalcols / 2) + 30;

    if(pixRow > brow && pixRow < trow) {
        if(pixCol > lcol && pixCol < rcol) {
    	    InOrOut = 3;
	}
    }
    return InOrOut;
}    

int InNose(int totalrows, int totalcols, int radius, int pixRow, int pixCol) {
    int InOut = 0;

    int centerX = totalcols / 2;
    int centerY = totalrows / 2;
    int dist = 0;

    dist = pow((pow(centerX - pixCol,2)) + (pow((centerY - (radius / 4)) - pixRow,2)), 0.5);
     
    if (dist < radius / 10) {
	    InOut = 1;
    }
    
    return InOut;
}    
