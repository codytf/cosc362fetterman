#/bin/bash
##### Author: Cody Fetterman ######

##############################################
##### A script to locate all .tex files  #####
##### compile them with pdflatex and     #####
##### then deletes all unneeded files    #####
##############################################

##### Warn the user #####
read -p "WARNING! All .log .out .aux and .idx files will be deleted from the current directory. Continue (y/n)?:  " -n 1 -r
echo 

if [[ $REPLY =~ ^[Yy]$ ]]
then 

##### Finds .tex files and compiles them #####
find -name *.tex > texfiles.txt

file=texfiles.txt

echo $file

lines=`cat $file`

for line in $lines
do
	pdflatex $line
	echo $line >> LatexCompiledReport.txt
	wc $line   >> LatexCompiledReport.txt
	echo 	   >> LatexCompiledReport.txt
done

##### Finds files to delete and removes them #####
find -name '*.log' -o -name '*.out' -o -name '*.aux' -o -name '*.idx' > deletefiles.txt

file2=deletefiles.txt

echo $file2

lines2=`cat $file2`

for line in $lines2
do
	rm $line
done

mkdir CompiledPDFs/
cp *.pdf CompiledPDFs/
rm *.pdf 

rm texfiles.txt
rm deletefiles.txt

echo 'Done. Compiled pdfs have been placed in CompiledPDFs directory. All pdfs in current directory have been copied there too.'
fi

