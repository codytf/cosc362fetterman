import re
import sys

""" This is a script to total the times from given .txt files with names and times in the following format:

					first_name last_name
					day_of_week nhr nmin
					...

    To Run:
	[user@machine directory]$ python MainFunction.py namesandtimes.txt ...
"""

# ==================================
# READ IN COMMAND LINE ARGUMENTS
# ==================================
if len(sys.argv) < 2:
    print("To Run: [user@machine directory]$ python MainFunction.py namesandtimes.txt ...")
# ==================================
# OPEN FILES AND BUILD LIST OF DATA
# ==================================
NamesAndTimes = []
HoursAndMinutes = []
new_person = False
total_time = 0

for file in sys.argv[1:]:
    NamesAndTimesData = open(file,"r")
    new_person = True
    total_time = 0
    for line in NamesAndTimesData.readlines():
        if new_person == True:
            NamesAndTimes.append(line)
            new_person = False
        else:
            HoursAndMinutes = list(map(int, re.findall('\d+', line)))
            total_time += (HoursAndMinutes[0]) * 60
            total_time += HoursAndMinutes[1]
    NamesAndTimes.append(total_time)

# ===============================
# OUTPUT NAMES AND TOTAL TIMES
# ===============================
fileOutput = open('Report.txt', 'w+')
fileOutput.write("-------------------------------\n")
fileOutput.write("Report of total times.\n")
fileOutput.write("-------------------------------\n")

for item in NamesAndTimes:
    if isinstance(item, int):
        hours = item / 60
        minutes = item % 60
        string = "{} hours {} minutes\n\n"
        item = string.format(int(hours), minutes)
    fileOutput.write(item)

fileOutput.close()
