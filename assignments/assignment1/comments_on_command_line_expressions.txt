ls		lists files and directories in current directory
ls -l 		a long listing of the files and directories in the current directory
ls -a		shows hidden '.' files
cd		used to change directories
cd ..		goes back one level
rm -r 		recursive remove, removes all files in a directory then the directory itself
rm *.log	removes all .log files in current directory
du		disk usage, provides information about how much space files use on the hard disk
mv		used to move a file from one directory to another, or rename the file
cp		used to copy a file from one directory to another
cp *.extension	copies all files with the specified extension
less <filename> similar to more but it does not need to read the entire input file at once
more <filename> used to display text one page at time for easier viewing on a terminal
whoami		displays the name of the user you are currently signed in as
passwd		used to change the password of a user
wd filename

When I used the diff command on the two files, one containing my full history of bash commands and then the one only containing the last 35 or so lines that detailed my exploration of the above commands, only the lines not containing the exploration of the above commands were shown. 	
