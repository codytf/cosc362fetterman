\contentsline {chapter}{\numberline {1}PiServer}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Section A: Installation}{3}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Step One: Enable Network Booting on Client Pi}{4}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Step Two: Install PiServer}{4}{subsection.1.1.2}
\contentsline {section}{\numberline {1.2}Section B: Configuring PiServer}{5}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Step One: Attach the client Pi to the Network}{5}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Step Two: Configure PiServer}{5}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}Uses for PiServer}{7}{subsection.1.2.3}
